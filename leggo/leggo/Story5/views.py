from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import Article
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from . import forms

def lst(request):
    articles = Article.objects.all().order_by('date')
    return render(request,"lst.html",{'articles':articles})

def detail(request,slug):
    article = Article.objects.get(slug=slug)
    return render(request,"detail.html",{'article':article})

def create(request):
    if request.method == "POST":
        form = forms.CreateArticle(request.POST, request.FILES)
        if form.is_valid():
            return redirect("articles:list")
    else:
        form = forms.CreateArticle()
    return render(request,"create.html",{'form':form})